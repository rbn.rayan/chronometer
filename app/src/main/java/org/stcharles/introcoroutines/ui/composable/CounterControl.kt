package org.stcharles.introcoroutines.ui.composable

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun CounterControl(
    modifier: Modifier = Modifier,
    startCounter: () -> Unit,
    stopCounter: () -> Unit,
    resetCounter: () -> Unit
) {
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.Center
    ) {
        Button(
            modifier = Modifier.padding(10.dp),
            onClick = startCounter
        ) {
            Text(text = "Start")
        }
        Button(
            modifier = Modifier.padding(10.dp),
            onClick = stopCounter
        ) {
            Text(text = "Stop")
        }
        Button(
            modifier = Modifier.padding(10.dp),
            onClick = resetCounter
        ) {
            Text(text = "↺")
        }
    }
}