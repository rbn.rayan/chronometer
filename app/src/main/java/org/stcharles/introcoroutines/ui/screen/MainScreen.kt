package org.stcharles.introcoroutines.ui.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import org.stcharles.introcoroutines.ui.composable.CounterControl
import org.stcharles.introcoroutines.ui.composable.CounterDisplay

@Composable
fun MainScreen(viewModel: MainScreenViewModel) {
    val counterValue by viewModel.counterValue.collectAsState()
    val secondHandAngle by viewModel.secondHandAngle.collectAsState()
    val minuteHandAngle by viewModel.minuteHandAngle.collectAsState()

    MainScreen(
        counterValue = counterValue,
        secondHandAngle = secondHandAngle,
        minuteHandAngle = minuteHandAngle,
        startCounter = viewModel::startCounting,
        stopCounter = viewModel::stopCounting,
        resetCounter = viewModel::resetCounter
    )
}

@Composable
fun MainScreen(
    counterValue: Float,
    secondHandAngle: Float,
    minuteHandAngle: Float,
    startCounter: () -> Unit,
    stopCounter: () -> Unit,
    resetCounter: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Row {
            CounterDisplay(
                counterValue = counterValue,
                secondHandAngle = secondHandAngle,
                minuteHandAngle = minuteHandAngle
            )
        }

        Spacer(modifier = Modifier.size(20.dp))

        Row {
            CounterControl(
                startCounter = startCounter,
                stopCounter = stopCounter,
                resetCounter = resetCounter
            )
        }
    }
}