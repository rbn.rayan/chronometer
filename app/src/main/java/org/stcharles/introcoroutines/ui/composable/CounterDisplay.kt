package org.stcharles.introcoroutines.ui.composable

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em

@Composable
fun CounterDisplay(
    modifier: Modifier = Modifier,
    counterValue: Float,
    secondHandAngle: Float,
    minuteHandAngle: Float
) {
    Box(
        modifier = modifier
            .padding(10.dp)
            .clip(CircleShape)
            .border(
                width = 5.dp,
                color = Color.Gray,
                shape = CircleShape
            )
            .background(Color.Black)
            .size(250.dp),
        contentAlignment = Alignment.Center
    ) {
        Canvas(modifier = Modifier.fillMaxSize()) {
            for (i in 60 downTo 0) {
                drawArc(
                    color = Color.White,
                    startAngle = 270f + i * 6f,
                    sweepAngle = 1f,
                    useCenter = true,
                    alpha = 1.0f,
                )
            }
            drawCircle(
                color = Color.Black,
                radius = 300f,
                alpha = 1.0f,
            )
            for (i in 0 until 12) {
                drawArc(
                    color = Color.White,
                    startAngle = 270f + i * 30f,
                    sweepAngle = 1f,
                    useCenter = true,
                    alpha = 1.0f,
                )
            }
            drawCircle(
                color = Color.Black,
                radius = 250f,
                alpha = 1.0f,
            )
            drawArc(
                color = Color.Blue,
                startAngle = secondHandAngle,
                sweepAngle = 2f,
                useCenter = true,
                alpha = 1.0f
            )
            drawArc(
                color = Color.Red,
                startAngle = minuteHandAngle,
                sweepAngle = 2f,
                useCenter = true,
                alpha = 1.0f
            )
        }
        Text(
            modifier = Modifier.padding(10.dp),
            text = "%.2f".format(counterValue),
            color = Color.White,
            fontSize = 10.em
        )
    }
}

@Preview(showBackground = true)
@Composable
fun CounterDisplayPreview() {
    CounterDisplay(
        counterValue = 0f,
        secondHandAngle = 0f,
        minuteHandAngle = 0f
    )
}