package org.stcharles.introcoroutines.ui.screen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

const val STARTING_ANGLE = 270f

class MainScreenViewModel: ViewModel() {
    private val _counterValue: MutableStateFlow<Float> = MutableStateFlow(0f)
    val counterValue: StateFlow<Float> = _counterValue

    private val _secondHandAngle: MutableStateFlow<Float> = MutableStateFlow(270f)
    val secondHandAngle: StateFlow<Float> = _secondHandAngle

    private val _minuteHandAngle: MutableStateFlow<Float> = MutableStateFlow(270f)
    val minuteHandAngle: StateFlow<Float> = _minuteHandAngle

    private var counterJob: Job? = null
    private var secondsHandJob: Job? = null
    private var minuteHandJob: Job? = null

    fun startCounting() {
        if (counterJob == null || counterJob?.isCancelled == true) {
            secondsHandJob = viewModelScope.launch(Dispatchers.Default) {
                while (true) {
                    delay(250L)
                    _secondHandAngle.value =
                        STARTING_ANGLE + _counterValue.value * (360f / 60f)
                }
            }

            minuteHandJob = viewModelScope.launch(Dispatchers.Default) {
                while (true) {
                    delay(1000L)
                    _minuteHandAngle.value =
                        STARTING_ANGLE + _counterValue.value * (360f / (60f * 60f))
                }
            }

            counterJob = viewModelScope.launch(Dispatchers.Default) {
                while (true) {
                    delay(10L)
                    _counterValue.value += 0.01f
                }
            }
        }
    }

    fun stopCounting() {
        counterJob?.cancel()
        secondsHandJob?.cancel()
        minuteHandJob?.cancel()
    }

    fun resetCounter() {
        _counterValue.value = 0f
        _secondHandAngle.value = STARTING_ANGLE
        _minuteHandAngle.value = STARTING_ANGLE
    }
}